$(document).ready(function() {
    $('button.btn').click(function(){
        var t = this;
        var currency_from = $('#currencyFrom').val();
        var currency_to = $('#currencyTo').val();
        var amount = parseFloat($('.from_amount').val());

        if (currency_from == currency_to)
        {
            alert('Select a different currency please');
            return;
        }

        if (isNaN(amount) || !amount)
        {
            alert('Wrong amount');
            return;
        }

        $.ajax('/', {
            type:'POST',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify({
                from: currency_from,
                to: currency_to,
                amount: amount
            }),
            success:function(data)
            {
                $('.to_amount').val(data.value);
            },
            error:function(jqXHR,textStatus,errorThrown)
            {
                alert(jqXHR.status + ' ' + errorThrown);
            }
        });
    });
});