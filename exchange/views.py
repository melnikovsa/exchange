

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer

from exchange.currency import Currency
from exchange.settings import EXCHANGE_CURRENCIES


class Exchange(APIView):
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    template_name = 'index.html'

    def get(self, request):
        return Response()

    def post(self, request):
        try:
            if request.data['from'] == request.data['to'] or not float(request.data['amount']) or \
                    request.data['from'] not in EXCHANGE_CURRENCIES or request.data['to'] not in EXCHANGE_CURRENCIES:
                return Response(status=400)

            value = Currency(request.data['from'], request.data['to'], float(request.data['amount'])).convert()

            print(value)
            # if cann't update exchange rates
            if not value:
                return Response(status=503)

            return Response({
                'result': True,
                'value': value
            })

        except Exception as e:
            print(e)
            return Response(status=400)
