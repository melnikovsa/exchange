

import requests
import redis
import time

from exchange.settings import EXCHANGE_API_URL, EXCHANGE_APP_ID, EXCHANGE_CURRENCIES, REDIS_SERVER, REDIS_PORT


class Currency:
    def __init__(self, currency_from, currency_to, amount):
        self.currency_from = currency_from
        self.currency_to = currency_to
        self.amount = amount
        self.redis = redis.Redis(host=REDIS_SERVER, port=REDIS_PORT)

    def convert(self):
        currency = self.get_currency()
        return currency * self.amount

    def get_currency(self):
        if not self.check_update_date() and not self.update_currencies():
            return 0

        return float(self.redis.get('{}'.format(self.currency_to))) / \
               float(self.redis.get('{}'.format(self.currency_from)))

    def get_currency_from_exchange(self):
        print('update from api')
        try:
            r = requests.get('{}latest.json?&app_id={}&symbols={}'.format(
                EXCHANGE_API_URL, EXCHANGE_APP_ID, ",".join(EXCHANGE_CURRENCIES)), timeout=5)
            data = r.json()
            print(data)
            return True, data['rates']
        except Exception as e:
            print(e)
            return False, None

    def check_update_date(self):
        updated_date = self.redis.get('updated:date')

        if updated_date is None or updated_date.decode('ascii') != self.get_today():
            print('need update')
            return False
        return True

    def update_currencies(self):
        # check lock
        if self.redis.get('updated:lock') is None:
            self.redis.set('updated:lock', 1)
            status, rates = self.get_currency_from_exchange()

            if not status:
                self.redis.delete('updated:lock')
                return False

            for currency in rates:
                self.redis.set('{}'.format(currency), rates[currency])

            self.redis.set('updated:date', self.get_today())
            self.redis.delete('updated:lock')
            return True
        else:
            # wait while write data to redis
            i = 0
            while i < 5:
                if self.redis.get('updated:lock') is None:
                    return self.check_update_date()
                time.sleep(1)
                i += 1
            return False

    @staticmethod
    def get_today():
        return time.strftime("%Y-%m-%d", time.localtime())
