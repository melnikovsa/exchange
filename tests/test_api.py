import redis
from django.test import TestCase
# from unittest import TestCase
# from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status

from exchange.settings import REDIS_SERVER, REDIS_PORT


class APITestCase(TestCase):

    def setUp(self):
        self.client = APIClient()

    def setup_databases(self, **kwargs):
        """ Override the database creation defined in parent class """
        pass

    def teardown_databases(self, old_config, **kwargs):
        """ Override the database teardown defined in parent class """
        pass

    def _post_teardown(self):
        """ Override the database teardown defined in parent class """
        pass

    def is_float(self, val):
        try:
            float(val)
            return True
        except ValueError:
            return False

    def test_index_page(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_exchange_api(self):
        response = self.client.post('/', {}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {
            'from': 'USD',
            'to': 'USD',
            'amount': 1
        }
        response = self.client.post('/', data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {
            'from': 'RUR',
            'to': 'USD',
            'amount': 1
        }
        response = self.client.post('/', data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {
            'from': 'USD',
            'to': 'RUR',
            'amount': 1
        }
        response = self.client.post('/', data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {
            'from': 'USD',
            'to': 'USD',
            'amount': 'rthr'
        }
        response = self.client.post('/', data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {
            'from': 'USD',
            'to': 'USD',
            'amount': 0
        }
        response = self.client.post('/', data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # test with clear rates
        r = redis.Redis(host=REDIS_SERVER, port=REDIS_PORT)
        r.delete('updated:date')
        r.delete('updated:lock')

        data = {
            'from': 'USD',
            'to': 'EUR',
            'amount': 1
        }
        response = self.client.post('/', data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('result' in response.data and response.data['result'])
        self.assertTrue('value' in response.data and self.is_float(response.data['value']) and
                        float(response.data['value']))

        data = {
            'from': 'USD',
            'to': 'EUR',
            'amount': 1
        }
        response = self.client.post('/', data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('result' in response.data and response.data['result'])
        self.assertTrue('value' in response.data and self.is_float(response.data['value']) and
                        float(response.data['value']))

        r.set('updated:lock', 1)
        r.delete('updated:date')
        data = {
            'from': 'USD',
            'to': 'EUR',
            'amount': 1
        }
        response = self.client.post('/', data, format="json")
        self.assertEqual(response.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
